/*!
 *  Bit&Black Helpers – Useful functions for JS you may like.
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */
/**
 * Calls a function only once for a given time.
 * When the time is over, the function may be called again.
 *
 * @param callback
 * @param timeToWait
 */
export declare const runOnce: (callback: any, timeToWait?: number) => void;
