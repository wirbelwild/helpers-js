"use strict";
/*!
 *  Bit&Black Helpers – Useful functions for JS you may like.
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */
exports.__esModule = true;
exports.stringToBoolean = void 0;
var ArrayHelper_1 = require("./ArrayHelper");
var stringToBoolean = function (input) {
    return (0, ArrayHelper_1.recurse)(input, function (input) {
        var inputConverted = input;
        if (typeof input === "string") {
            inputConverted = input.toLowerCase();
        }
        switch (inputConverted) {
            case "true":
                return true;
            case "false":
                return false;
            case "null":
                return null;
            default:
                return input;
        }
    });
};
exports.stringToBoolean = stringToBoolean;
