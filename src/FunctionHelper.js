"use strict";
/*!
 *  Bit&Black Helpers – Useful functions for JS you may like.
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */
exports.__esModule = true;
exports.runOnce = void 0;
var timeout = null;
/**
 * Calls a function only once for a given time.
 * When the time is over, the function may be called again.
 *
 * @param callback
 * @param timeToWait
 */
var runOnce = function (callback, timeToWait) {
    if (timeToWait === void 0) { timeToWait = 250; }
    clearTimeout(timeout);
    timeout = setTimeout(function () {
        callback();
    }, timeToWait);
};
exports.runOnce = runOnce;
