"use strict";
/*!
 *  Bit&Black Helpers – Useful functions for JS you may like.
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
exports.__esModule = true;
exports.recurse = exports.getElementsUnified = exports.pushUnique = exports.arrayAddOrRemove = void 0;
/**
 * Adds an element to an array if it's not part of it, or removes an element if it is already set.
 *
 * @param input
 * @param value
 * @return {boolean}
 */
var arrayAddOrRemove = function (input, value) {
    var index = input.indexOf(value);
    var isExisting = -1 !== index;
    isExisting
        ? input.splice(index, 1)
        : input.push(value);
    return !isExisting;
};
exports.arrayAddOrRemove = arrayAddOrRemove;
/**
 * Pushes items into an array and uniques them.
 *
 * @param existingValues {Array}
 * @param newValues {Array}
 * @return {Array}
 * @deprecated This function has been deprecated. Please use `getElementsUnified` instead.
 * @see {@link getElementsUnified}
 * @todo Remove in v1.0.
 */
var pushUnique = function (existingValues, newValues) {
    return (0, exports.getElementsUnified)(existingValues, newValues);
};
exports.pushUnique = pushUnique;
/**
 * Pushes items into an array and uniques them.
 *
 * @param existingValues {Array}
 * @param newValues {Array}
 * @return {Array}
 */
var getElementsUnified = function (existingValues, newValues) {
    return __spreadArray([], __read(new Set(__spreadArray(__spreadArray([], __read(existingValues), false), __read(newValues), false))), false);
};
exports.getElementsUnified = getElementsUnified;
var recurse = function (input, callback) {
    var e_1, _a;
    var inputCopy = input;
    if (null === inputCopy || "undefined" === typeof inputCopy) {
        return inputCopy;
    }
    if (!Array.isArray(inputCopy) && typeof inputCopy !== "object") {
        return callback(inputCopy, null);
    }
    try {
        for (var _b = __values(Object.entries(inputCopy)), _c = _b.next(); !_c.done; _c = _b.next()) {
            var _d = __read(_c.value, 2), key = _d[0], value = _d[1];
            inputCopy[key] = (0, exports.recurse)(value, callback);
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (_c && !_c.done && (_a = _b["return"])) _a.call(_b);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return inputCopy;
};
exports.recurse = recurse;
