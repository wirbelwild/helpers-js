/*!
 *  Bit&Black Helpers – Useful functions for JS you may like.
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */

let timeout = null;

/**
 * Calls a function only once for a given time.
 * When the time is over, the function may be called again.
 *
 * @param callback
 * @param timeToWait
 */
export const runOnce = (callback, timeToWait: number = 250) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
        callback();
    }, timeToWait);
}