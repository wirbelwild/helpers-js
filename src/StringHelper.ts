/*!
 *  Bit&Black Helpers – Useful functions for JS you may like.
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */

import { recurse } from "./ArrayHelper";

export const stringToBoolean = (input: any): any => {
    return recurse(
        input,
        (input: any) => {
            let inputConverted: any = input;

            if (typeof input === "string") {
                inputConverted = input.toLowerCase();
            }

            switch (inputConverted) {
                case "true":
                    return true;
                case "false":
                    return false;
                case "null":
                    return null;
                default:
                    return input;
            }
        },
    )
};