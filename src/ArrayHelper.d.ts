/*!
 *  Bit&Black Helpers – Useful functions for JS you may like.
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */
/**
 * Adds an element to an array if it's not part of it, or removes an element if it is already set.
 *
 * @param input
 * @param value
 * @return {boolean}
 */
export declare const arrayAddOrRemove: (input: Array<any>, value: any) => boolean;
/**
 * Pushes items into an array and uniques them.
 *
 * @param existingValues {Array}
 * @param newValues {Array}
 * @return {Array}
 * @deprecated This function has been deprecated. Please use `getElementsUnified` instead.
 * @see {@link getElementsUnified}
 * @todo Remove in v1.0.
 */
export declare const pushUnique: (existingValues: Array<any>, newValues: Array<any>) => Array<any>;
/**
 * Pushes items into an array and uniques them.
 *
 * @param existingValues {Array}
 * @param newValues {Array}
 * @return {Array}
 */
export declare const getElementsUnified: (existingValues: Array<any>, newValues: Array<any>) => Array<any>;
export declare const recurse: (input: any, callback: (value: any, key: any) => any) => any;
