/*!
 *  Bit&Black Helpers – Useful functions for JS you may like.
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */

/**
 * Adds an element to an array if it's not part of it, or removes an element if it is already set.
 *
 * @param input
 * @param value
 * @return {boolean}
 */
export const arrayAddOrRemove = (input: Array<any>, value: any): boolean => {
    const index = input.indexOf(value);
    const isExisting = -1 !== index;

    isExisting
        ? input.splice(index, 1)
        : input.push(value)
    ;

    return !isExisting;
};

/**
 * Pushes items into an array and uniques them.
 *
 * @param existingValues {Array}
 * @param newValues {Array}
 * @return {Array}
 * @deprecated This function has been deprecated. Please use `getElementsUnified` instead.
 * @see {@link getElementsUnified}
 * @todo Remove in v1.0.
 */
export const pushUnique = (existingValues: Array<any>, newValues: Array<any>): Array<any> => {
    return getElementsUnified(existingValues, newValues);
};

/**
 * Pushes items into an array and uniques them.
 *
 * @param existingValues {Array}
 * @param newValues {Array}
 * @return {Array}
 */
export const getElementsUnified = (existingValues: Array<any>, newValues: Array<any>): Array<any> => {
    return [
        ...new Set([
            ...existingValues,
            ...newValues
        ])
    ];
};

export const recurse = (input: any, callback: (value: any, key: any) => any): any => {
    let inputCopy = input;

    if (null === inputCopy || "undefined" === typeof inputCopy) {
        return inputCopy;
    }

    if (!Array.isArray(inputCopy) && typeof inputCopy !== "object") {
        return callback(inputCopy, null);
    }

    for (const [key, value] of Object.entries(inputCopy)) {
        inputCopy[key] = recurse(value, callback);
    }

    return inputCopy;
}
