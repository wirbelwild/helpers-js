"use strict";
/*!
 *  Bit&Black Helpers – Useful functions for JS you may like.
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */
exports.__esModule = true;
var ArrayHelper_1 = require("../src/ArrayHelper");
it("Test arrayAddOrRemove", function () {
    var input = [
        'A',
        'B',
        'C'
    ];
    var inputMerged1 = (0, ArrayHelper_1.arrayAddOrRemove)(input, 'C');
    expect(input)
        .toStrictEqual([
        'A',
        'B'
    ]);
    expect(inputMerged1).toBe(false);
    var inputMerged2 = (0, ArrayHelper_1.arrayAddOrRemove)(input, 'C');
    expect(input)
        .toStrictEqual([
        'A',
        'B',
        'C'
    ]);
    expect(inputMerged2).toBe(true);
});
it("Test getElementsUnified", function () {
    var input = [
        'A',
        'B',
        'C'
    ];
    var inputAdditional = [
        'C',
        'D'
    ];
    var inputMerged = (0, ArrayHelper_1.getElementsUnified)(input, inputAdditional);
    expect(inputMerged)
        .toStrictEqual([
        'A',
        'B',
        'C',
        'D'
    ]);
});
describe("Can recursively handle", function () {
    it("simple input", function () {
        var input = [
            'A',
            'B',
            'C'
        ];
        var inputConverted = (0, ArrayHelper_1.recurse)(input, function (input) {
            return input.toLowerCase();
        });
        expect(inputConverted)
            .toStrictEqual([
            'a',
            'b',
            'c',
        ]);
    });
    it("nested input", function () {
        var input = [
            [
                'A',
                'B',
                'C'
            ],
            [
                'A',
                'B',
                'C'
            ]
        ];
        var inputConverted = (0, ArrayHelper_1.recurse)(input, function (input) {
            return input.toLowerCase();
        });
        expect(inputConverted)
            .toStrictEqual([
            [
                'a',
                'b',
                'c',
            ],
            [
                'a',
                'b',
                'c',
            ]
        ]);
    });
    it("null", function () {
        var input = null;
        var outputExpected = null;
        var output = (0, ArrayHelper_1.recurse)(input, function (input) {
            return input.toLowerCase();
        });
        expect(output).toStrictEqual(outputExpected);
    });
    it("undefined", function () {
        var input = undefined;
        var outputExpected = undefined;
        var output = (0, ArrayHelper_1.recurse)(input, function (input) {
            return input.toLowerCase();
        });
        expect(output).toStrictEqual(outputExpected);
    });
});
