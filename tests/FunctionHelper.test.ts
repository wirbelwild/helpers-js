/*!
 *  Bit&Black Helpers – Useful functions for JS you may like.
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */

import { runOnce } from "../src/FunctionHelper";

it("Test runOnce", async () => {
    let count = 0;

    const incrementCounter = () => {
        ++count;
    };

    expect(count).toBe(0);

    incrementCounter();

    expect(count).toBe(1);

    runOnce(incrementCounter);
    runOnce(incrementCounter);
    runOnce(incrementCounter);

    await new Promise(
        (resolve) => setTimeout(resolve, 500)
    );

    expect(count).toBe(2);

    runOnce(incrementCounter);
    runOnce(incrementCounter);
    runOnce(incrementCounter);

    await new Promise(
        (resolve) => setTimeout(resolve, 500)
    );

    expect(count).toBe(3);
});