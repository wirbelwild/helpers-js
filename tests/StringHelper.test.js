"use strict";
/*!
 *  Bit&Black Helpers – Useful functions for JS you may like.
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */
exports.__esModule = true;
var StringHelper_1 = require("../src/StringHelper");
describe("Can convert string to boolean", function () {
    it("with true", function () {
        var input = "true";
        var outputExpected = true;
        var output = (0, StringHelper_1.stringToBoolean)(input);
        expect(output).toStrictEqual(outputExpected);
    });
    it("with false", function () {
        var input = "false";
        var outputExpected = false;
        var output = (0, StringHelper_1.stringToBoolean)(input);
        expect(output).toStrictEqual(outputExpected);
    });
    it("with null", function () {
        var input = "null";
        var outputExpected = null;
        var output = (0, StringHelper_1.stringToBoolean)(input);
        expect(output).toStrictEqual(outputExpected);
    });
    it("with string", function () {
        var input = "Hello World!";
        var outputExpected = "Hello World!";
        var output = (0, StringHelper_1.stringToBoolean)(input);
        expect(output).toStrictEqual(outputExpected);
    });
    it("with number", function () {
        var input = 123;
        var outputExpected = 123;
        var output = (0, StringHelper_1.stringToBoolean)(input);
        expect(output).toStrictEqual(outputExpected);
    });
    it("with nested input", function () {
        var input = [
            "true",
            [
                123,
                "null",
            ],
            "falsy",
        ];
        var outputExpected = [
            true,
            [
                123,
                null,
            ],
            "falsy",
        ];
        var output = (0, StringHelper_1.stringToBoolean)(input);
        expect(output).toStrictEqual(outputExpected);
    });
});
