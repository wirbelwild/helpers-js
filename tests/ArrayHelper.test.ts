/*!
 *  Bit&Black Helpers – Useful functions for JS you may like.
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */

import { arrayAddOrRemove, getElementsUnified, recurse } from "../src/ArrayHelper";

it("Test arrayAddOrRemove", () => {

    const input = [
        'A',
        'B',
        'C'
    ];

    const inputMerged1 = arrayAddOrRemove(input, 'C');

    expect(input)
        .toStrictEqual([
            'A',
            'B'
        ])
    ;
    
    expect(inputMerged1).toBe(false);

    const inputMerged2 = arrayAddOrRemove(input, 'C');
    
    expect(input)
        .toStrictEqual([
            'A',
            'B',
            'C'
        ])
    ;

    expect(inputMerged2).toBe(true);

});

it("Test getElementsUnified", () => {

    const input = [
        'A',
        'B',
        'C'
    ];

    const inputAdditional = [
        'C',
        'D'
    ];

    const inputMerged = getElementsUnified(input, inputAdditional);

    expect(inputMerged)
        .toStrictEqual([
            'A',
            'B',
            'C',
            'D'
        ])
    ;
});

describe("Can recursively handle", () => {
    it("simple input", () => {
        const input = [
            'A',
            'B',
            'C'
        ];

        const inputConverted = recurse(
            input,
            (input) => {
                return input.toLowerCase();
            },
        );

        expect(inputConverted)
            .toStrictEqual([
                'a',
                'b',
                'c',
            ])
        ;
    });

    it("nested input", () => {
        const input = [
            [
                'A',
                'B',
                'C'
            ],
            [
                'A',
                'B',
                'C'
            ]
        ];

        const inputConverted = recurse(
            input,
            (input) => {
                return input.toLowerCase();
            },
        );

        expect(inputConverted)
            .toStrictEqual([
                [
                    'a',
                    'b',
                    'c',
                ],
                [
                    'a',
                    'b',
                    'c',
                ]
            ])
        ;
    });

    it("null", () => {
        const input = null;
        const outputExpected = null;
        const output = recurse(
            input,
            (input) => {
                return input.toLowerCase();
            },
        );

        expect(output).toStrictEqual(outputExpected);
    });

    it("undefined", () => {
        const input = undefined;
        const outputExpected = undefined;
        const output = recurse(
            input,
            (input) => {
                return input.toLowerCase();
            },
        );

        expect(output).toStrictEqual(outputExpected);
    });
});
