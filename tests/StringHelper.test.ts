/*!
 *  Bit&Black Helpers – Useful functions for JS you may like.
 *
 *  @copyright Copyright (c) Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */

import { stringToBoolean } from "../src/StringHelper";

describe("Can convert string to boolean", () => {
    it("with true", () => {
        const input = "true";
        const outputExpected = true;
        const output = stringToBoolean(input);

        expect(output).toStrictEqual(outputExpected);
    });

    it("with false", () => {
        const input = "false";
        const outputExpected = false;
        const output = stringToBoolean(input);

        expect(output).toStrictEqual(outputExpected);
    });

    it("with null", () => {
        const input = "null";
        const outputExpected = null;
        const output = stringToBoolean(input);

        expect(output).toStrictEqual(outputExpected);
    });

    it("with string", () => {
        const input = "Hello World!";
        const outputExpected = "Hello World!";
        const output = stringToBoolean(input);

        expect(output).toStrictEqual(outputExpected);
    });

    it("with number", () => {
        const input = 123;
        const outputExpected = 123;
        const output = stringToBoolean(input);

        expect(output).toStrictEqual(outputExpected);
    });

    it("with nested input", () => {
        const input = [
            "true",
            [
                123,
                "null",
            ],
            "falsy",
        ];
        const outputExpected = [
            true,
            [
                123,
                null,
            ],
            "falsy",
        ];
        const output = stringToBoolean(input);

        expect(output).toStrictEqual(outputExpected);
    });
});