[![Codacy Badge](https://app.codacy.com/project/badge/Grade/cd5bd05ed0bc4939944078adc0381dfb)](https://www.codacy.com/bb/wirbelwild/helpers-js/dashboard)
[![npm version](https://badge.fury.io/js/bitandblack-helpers.svg)](https://badge.fury.io/js/bitandblack-helpers)

# Helpers

Useful functions for JS you may like.

## Installation

This library is made for the use with [Node](https://www.npmjs.com/package/bitandblack-helpers). Add it to your project by running `$ npm install bitandblack-helpers` or `$ yarn add bitandblack-helpers`.

## Use

There are useful functions to handle:

-   Arrays
    -   `arrayAddOrRemove` Adds an element to an array if it's not part of it, or removes an element if it is already set.
    -   `getElementsUnified` Pushes items into an array and returns a unified one.
    -   `recurse` Runs a function on an input, no matter if it's a string or an array.

-   Functions
    -   `runOnce` Calls a function only once for a given time. When the time is over, the function may be called again.

-   Strings
    -   `stringToBoolean`. Converts the input to boolean if possible. 

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).
